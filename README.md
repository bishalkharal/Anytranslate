# Anytranslate
Anytranslate is an wrapper API for google translate


This python program fetches the data from [Google translate](https://translate.google.com/) and provides them in a pythonic and usable way


## About

This is a API wrapper for Google Translate.

## How to use?
Install poetry on your system from [here](https://python-poetry.org/docs/)
```sh
poetry shell
poetry install
```

And finally start the program by 
```sh
uvicorn trans_api:app --reaload
```

## Why use this?

- It is asynchronous.
- Data can be fetched in json format from endpoint
















